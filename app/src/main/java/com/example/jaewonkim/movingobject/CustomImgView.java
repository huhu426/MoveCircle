package com.example.jaewonkim.movingobject;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Jaewon Kim on 2016-04-28.
 */
public class CustomImgView extends ImageView {

    int x=100;
    int y=100;
    float radius = 25;

    public CustomImgView(Context context) {
        super(context);
    }

    public CustomImgView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomImgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomImgView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN :
            case MotionEvent.ACTION_MOVE :
            case MotionEvent.ACTION_UP   :
                x = (int)event.getX();
                y = (int)event.getY();
                invalidate(); // 화면을 다시 그려줘라 => onDraw() 호출해준다
        }
        return true;
    }

    public void setPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
        canvas.drawCircle(x, y, radius, paint);
    }
}
