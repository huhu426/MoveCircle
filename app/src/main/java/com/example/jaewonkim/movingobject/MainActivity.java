package com.example.jaewonkim.movingobject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    CustomImgView imageView;
    TextView fpsTV;

    long fpsStartTime = 0L;             // Frame 시작 시간
    int frameCnt = 0;                      // 돌아간 Frame 갯수
    double timeElapsed = 0.0f;         // 그 동안 쌓인 시간 차이

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BitmapFactory.Options myOptions = new BitmapFactory.Options();
        myOptions.inDither = true;
        myOptions.inScaled = false;
        myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        myOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.image1,myOptions);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);

        Bitmap workingBitmap = Bitmap.createBitmap(bitmap);
        Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);


        Canvas canvas = new Canvas(mutableBitmap);
//        canvas.drawCircle(60, 50, 25, paint);

        fpsTV = (TextView)findViewById(R.id.textView);
        imageView = (CustomImgView)findViewById(R.id.imageView);
        imageView.setAdjustViewBounds(true);
        imageView.setImageBitmap(mutableBitmap);
        fpsStartTime = System.currentTimeMillis();
    }

    @Override
    protected void onResume() {
        super.onResume();

        int x = 10;
        int y = 10;

        for(int i = 0 ; i < 1000 ; i++) {
            imageView.setPoint(x+i, y);
            imageView.invalidate();

            long fpsEndTime = System.currentTimeMillis();

            float timeDelta = (fpsEndTime - fpsStartTime) * 0.001f;
            frameCnt++;
            timeElapsed += timeDelta;

            if(timeElapsed >= 1.0f){
                float fps = (float)(frameCnt/timeElapsed);
                Log.d("fps", "fps : " + fps);
                fpsTV.setText("fps : " + fps);
                frameCnt = 0;
                timeElapsed = 0.0f;
            }

            fpsStartTime = System.currentTimeMillis();

        }


//        while(true) {
//            imageView.setPoint(x, y);
//            imageView.invalidate();
//
//
//            long fpsEndTime = System.currentTimeMillis();
//
//            float timeDelta = (fpsEndTime - fpsStartTime) * 0.001f;
//            frameCnt++;
//            timeElapsed += timeDelta;
//
//            if(timeElapsed >= 1.0f){
//                float fps = (float)(frameCnt/timeElapsed);
//                Log.d("fps", "fps : " + fps);
//                fpsTV.setText("fps : " + fps);
//                frameCnt = 0;
//                timeElapsed = 0.0f;
//            }
//
//            fpsStartTime = System.currentTimeMillis();
//        }
    }

}
